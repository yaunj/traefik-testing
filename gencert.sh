#!/bin/bash
# Generate TLS cert

CN=${1:-localhost}
SAN=${2:-DNS:${CN},IP:127.0.0.1}

openssl req -x509 -newkey rsa:4096 -sha256 -days 10 -nodes \
  -keyout "certs/${CN}.key" -out "certs/${CN}.pem" -subj "/CN=${CN}" \
  -addext "subjectAltName=${SAN}"
